package am.matveev.demo.Demo.repositories;

import am.matveev.demo.Demo.entity.DemoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemoRepository extends JpaRepository<DemoEntity,Long>{
}
