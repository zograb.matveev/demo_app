package am.matveev.demo.Demo.controllers;

import am.matveev.demo.Demo.entity.DemoEntity;
import am.matveev.demo.Demo.services.DemoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/demo")
@RestController
@RequiredArgsConstructor
public class DemoController{

    private final DemoService demoService;

    @GetMapping()
    public List<DemoEntity> findAll(){
        return demoService.findAll();
    }

    @GetMapping("/{id}")
    public DemoEntity findOne(@PathVariable long id){
        return demoService.findOne(id);
    }

    @PostMapping()
    public DemoEntity create(@RequestBody DemoEntity demoEntity){
        return demoService.create(demoEntity);
    }
}
