package am.matveev.demo.Demo.services;

import am.matveev.demo.Demo.entity.DemoEntity;
import am.matveev.demo.Demo.repositories.DemoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DemoService{

    private final DemoRepository demoRepository;

    @Transactional(readOnly = true)
    public List<DemoEntity> findAll(){
        List<DemoEntity> demoEntities = demoRepository.findAll();
        return demoEntities;
    }

    @Transactional(readOnly = true)
    public DemoEntity findOne(Long id){
        DemoEntity demoEntity = demoRepository.findById(id)
                .orElseThrow(RuntimeException::new);
        return demoEntity;
    }

    @Transactional
    public DemoEntity create(DemoEntity demoEntity){
        demoRepository.save(demoEntity);
        return demoEntity;
    }
}
