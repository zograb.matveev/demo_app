FROM openjdk:17
WORKDIR /app
COPY target/Demo-0.0.1-SNAPSHOT.jar Demo-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD ["java", "-jar", "Demo-0.0.1-SNAPSHOT.jar"]
